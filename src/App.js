import './App.css';
import {useEffect, useState} from "react";

const useObservable = (observable, defaultValue) => {
    const [state, setState] = useState(defaultValue);
    useEffect(() => {
        if (observable) {
            const subscription = observable.subscribe((nextValue) =>
                setState(nextValue || defaultValue)
            );
            return () => subscription.unsubscribe();
        }
    }, [observable]);

    return state;
};

const App = ({ name, jsonData, jsonDataInString, onInputChange }) => {
    const nameValue = useObservable(name, "")
    const jsonDataValue = useObservable(jsonData, {})
    const jsonDataInStringValue = useObservable(jsonDataInString, {})

  return (
    <div className="App" style={{ border: "1px solid black", backgroundColor: "yellow", padding: "10px"}}>
      <h1>Component content</h1>
      <div>Application name: <b>{nameValue}</b></div>
      <div>data: <b>{jsonDataValue.name}</b></div>
      <div>data as string: <b>{jsonDataInStringValue.name}</b></div>
      component input: <input onChange={onInputChange} />
    </div>
  );
}

export default App;

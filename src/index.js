import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import App2 from './App2';
import reportWebVitals from './reportWebVitals';
import {BehaviorSubject, Subject} from "rxjs";

class WebComponentApp extends HTMLElement {

    static get observedAttributes() {
        return ['name', 'data2']; // we are listening to those attributes
    }

    constructor() {
        super()
        this.name$ = new BehaviorSubject()
        this.jsonData$ = new BehaviorSubject()
        this.jsonDataInString$ = new BehaviorSubject()
    }

    connectedCallback() {
        const mountPoint = document.createElement('span');
        this.attachShadow({ mode: 'closed' }).appendChild(mountPoint);

        const onInputChange = e => {
            mountPoint.dispatchEvent(new CustomEvent('component-input', {
                bubbles: true,
                composed: true,
                detail: {
                    value: e.target.value,
                    doCommand: () => {
                        console.info("command triggered");
                    }
                }
            }));
        }

        ReactDOM.render(<App
            name={this.name$}
            jsonData={this.jsonData$}
            jsonDataInString={this.jsonDataInString$}
            onInputChange={onInputChange}
        />, mountPoint);

        this.name$.next(this.getAttribute('name'))
        this.jsonDataInString$.next(JSON.parse(this.getAttribute('data2')))
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (oldValue !== newValue) {
            switch(name) {
                case "name":
                    this.name$.next(newValue)
                    break;
                case "data2":
                    this.jsonDataInString$.next(JSON.parse(newValue))
                    break;
            }
        }
    }

    setJsonData(someData) {
        this.jsonData$.next(someData)
    }
}
customElements.define('my-tag', WebComponentApp);



ReactDOM.render(
  <React.StrictMode>
    <App2 />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

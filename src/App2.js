import './App.css';
import {useEffect, useRef, useState} from "react";


const App2 = () => {
    const [webComponentInput, setWebComponentInput] = useState("");
   const ref = useRef();

   useEffect(() => {
       if (ref && ref.current) {
           console.info(ref)

           const onComponentInput = (e) => {
               setWebComponentInput(e.detail.value);
               e.detail.doCommand();
           }

           ref.current.addEventListener("component-input", onComponentInput)

           return () => {
               ref.current.removeEventListener("component-input", onComponentInput);
           }
       }
   }, [ref && ref.current])

    const onChangeData = () => ref.current.setJsonData({ name: "this is my data!!" + Math.random() });

  return (
    <>
        <h1>Outside react content</h1>

        <div>
            component input: {webComponentInput}
        </div>


        <button onClick={onChangeData}>change data</button>
        <my-tag name="karel" data2='{ "name": "this is string data.." }' ref={ref}></my-tag>
    </>
  );
}

export default App2;
